import re
import arpreq
import hashlib

from apps.login.models import UsuarioModel, TerminalModel


def encriptar_hash256(cadena):
    '''
    El parametro se encripta con hash256
    '''
    encriptacion = hashlib.sha256()
    encriptacion.update(cadena.encode('utf-8'))
    resultado = encriptacion.hexdigest()
    return resultado


def limpiar_cadena(cadena):
    '''
    Los parametros ingresado por el usuario se hace mantenimiento:
        - Borrar los espacios en blanco al inicio y fin
    '''
    resultado = cadena.strip()
    return resultado


def validar_login(usuario, password):
    '''
    Pasos para poder verificar usuario y password:
        - Se encripta el password
        - Se conecta con la base de datos
        - Verificamos credenciales de usuario
        - Verificar si el usuario esta activo
    '''
    resultado = {
                    "error": True,
                    "mensaje": "",
                    "datos": {}
                }
    password = encriptar_hash256(password)
    lista_usuarios = UsuarioModel.select().where(UsuarioModel.usuario == usuario,
                                                UsuarioModel.password == password)

    if lista_usuarios.exists():
        usuario = lista_usuarios.first()
        if usuario.esta_activo:
            resultado['error'] = False
            resultado['datos'] = usuario
        else:
            resultado['mensaje'] = "Usuario inactivo"
    else:
        resultado['mensaje'] = "Usuario y/o password incorrectos"
    return resultado


def validar_formato_mac(mac):
    expresion = re.compile(r'''[0-9a-f]{2}([:])
                          [0-9a-f]{2}
                          (\1[0-9a-f]{2}){4}$''', re.X)
    resultado = expresion.match(mac)
    return resultado


def validar_formato_ip(ip):
    expresion = re.compile(r'''^(?:(?:25[0-5]
                            |2[0-4][0-9]
                            |[1]?[0-9][0-9]?)\.){3}
                            (?:25[0-5]
                            |2[0-4][0-9]
                            |[1]?[0-9][0-9]?)$''', re.X)
    resultado = expresion.match(ip)
    return resultado


def validar_mac(mac):
    '''
    Pasos para poder verificar mac:
        - Se verifica si existe un registro con mac ethernet
        - Se verifica si existe un registro con mac wireless
        - Se conecta con la base de datos
        - Verificamos credenciales de usuario
    '''
    resultado = {
                "error": True,
                "mensaje": "",
                "datos": {}
            }
    if validar_formato_mac(mac):
        terminales = TerminalModel.buscar_por_mac_en_ethernet_wireless(mac)
        if terminales['error']:
            resultado['mensaje'] = terminales['mensaje']
        else:
            resultado['error'] = False
            resultado['datos'] = terminales['datos']
    else:
        resultado['mensaje'] = 'Formato de M.A.C. invalido'

    return resultado

def obtener_mac_por_arp(ip):
    resultado = {
                "error": True,
                "mensaje": "",
                "datos": {}
            }

    if validar_formato_ip(ip):
        mac = arpreq.arpreq(ip)
        if mac:
            resultado["error"] = False
            resultado["datos"] = {'mac':mac}
        else:
            resultado['mensaje'] = "Terminal no esta conectado"
    else:
        resultado['mensaje'] = "formato de IP invalido"

    return resultado



def buscar_terminal_por_ip_mac(ip, mac):
    resultado = {
                "error": True,
                "mensaje": "",
                "datos": {}
            }
    resultado_terminales = TerminalModel.buscar_por_ip_mac(ip, mac)
    if resultado_terminales:
        if len(resultado_terminales) > 1:
            resultado['mensaje'] = "Error con su registro de terminal(2)"
        else:
            terminal = resultado_terminales.first()
            if terminal.esta_activo:
                resultado['error'] = False
                resultado['datos'] = terminal
            else:
                resultado['mensaje'] = "Terminal está inactivo"
    else:
        resultado['mensaje'] = "Terminal no está registrado"
    return resultado
