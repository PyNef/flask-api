from peewee import *
from config.database import gestagro_db

class BaseModel(Model):
    class Meta:
        database = gestagro_db


class UsuarioModel(BaseModel):
    usuario_id = CharField(primary_key=True)
    usuario = CharField()
    password = CharField()
    esta_activo = BooleanField(default=True)

    class Meta:
        db_table = 'usuario'


class TerminalModel(BaseModel):
    terminal_id = CharField(primary_key=True)
    ip_ethernet = CharField()
    mac_ethernet = CharField()
    ip_wireless = CharField()
    mac_wireless = CharField()
    esta_activo = BooleanField(default=True)

    @classmethod
    def buscar_por_ip_mac(cls, ip, mac):
        ethernet = cls.select().where(cls.ip_ethernet == ip, cls.mac_ethernet == mac)
        wireless = cls.select().where(cls.ip_wireless == ip, cls.mac_wireless == mac)
        query = ethernet + wireless
        return query

    class Meta:
        db_table = 'terminal'
