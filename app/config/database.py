import os
from dotenv import load_dotenv
from peewee import *

load_dotenv()
load_dotenv(verbose=True, dotenv_path='.env')


gestagro_db = MySQLDatabase(
                os.getenv("DB_NAME"),
                user = os.getenv("DB_USER"),
                passwd = os.getenv("DB_PASSWD"),
                host = os.getenv("DB_HOST"),
                port = int(os.getenv("DB_PORT")),
            )
