import os
import datetime
from flask import Flask, request
from flask_restful import Api, Resource

from flask_jwt_extended import JWTManager
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_required,
    jwt_refresh_token_required,
    get_jwt_identity, get_raw_jwt
)


from apps.login.functions import *
from apps.login.models import *

APP = Flask(__name__)
APP.config.from_object(__name__)
API = Api(APP)


"""
    Iniciando las conecciones a la base de datos
"""
@APP.before_request
def _db_connect():
    gestagro_db.connect()


@APP.teardown_request
def _db_close(exc):
    if not gestagro_db.is_closed():
        gestagro_db.close()


"""
    Agregando variables globales para el Json Web Token
"""
APP.config['JWT_SECRET_KEY'] = os.getenv("JWT_SECRET_KEY")
APP.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(seconds=180)
jwt = JWTManager(APP)



"""
    Resources
"""
class Login(Resource):
    def post(self):
        datos_json = request.get_json()
        usuario = datos_json.get('usuario')
        password = datos_json.get('password')
        ip = datos_json.get('ip')

        resultado = {
                        "error": True,
                        "mensaje": "",
                        "datos": {}
                    }
        print(usuario,password,ip)
        if not usuario or not password or not ip:
            resultado["mensaje"] = "Datos incompletos"
            return resultado

        resultado_login = validar_login(usuario, password)
        if resultado_login['error']:
            return resultado_login

        resultado_mac = obtener_mac_por_arp(ip)
        if resultado_mac['error']:
            return resultado_mac

        mac = resultado_mac['datos']['mac']
        resultado_terminales = buscar_terminal_por_ip_mac(ip, mac)
        if resultado_terminales['error']:
            return resultado_terminales

        access_token = create_access_token(identity=usuario)
        refresh_token = create_refresh_token(identity = usuario)
        return {
            'mac_id': resultado_terminales['datos'].terminal_id,
            'mensaje': 'Ingreso como {}'.format(usuario),
            'token_acceso': access_token,
            'token_actualizacion': refresh_token
            }


class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        access_token = create_access_token(identity = current_user)
        return {'access_token': access_token}


class PingResource(Resource):
    @jwt_required
    def get(self):
        return {
            'Pong': 42
        }


class InitResource(Resource):
    def get(self):
        return {
            'init': 'flask'
        }


API.add_resource(Login, '/login/')
API.add_resource(InitResource, '/')
API.add_resource(PingResource, '/test/')
API.add_resource(TokenRefresh, '/token_refresh/')


if __name__ == '__main__':
    APP.run(host='0.0.0.0', port=5000, debug=True)
